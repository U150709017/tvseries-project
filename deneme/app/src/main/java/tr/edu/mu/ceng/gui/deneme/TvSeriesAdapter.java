package tr.edu.mu.ceng.gui.deneme;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

public class TvSeriesAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<TvSeries> mTvSeriesList;

    public TvSeriesAdapter(Activity activity,List<TvSeries> TvSeries){
        mInflater = (LayoutInflater) activity.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        mTvSeriesList = TvSeries;
    }

    @Override
    public int getCount() {
        return mTvSeriesList.size();
    }

    @Override
    public Object getItem(int position) {
        return mTvSeriesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView;
        rowView = mInflater.inflate(R.layout.row_layout, null);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.list_view_image);
        TvSeries tvSeries = mTvSeriesList.get(position);
        imageView.setImageResource(tvSeries.getImageId());
        return rowView;
    }
}
