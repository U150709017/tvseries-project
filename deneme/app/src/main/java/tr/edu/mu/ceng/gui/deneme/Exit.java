package tr.edu.mu.ceng.gui.deneme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Exit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit);

        Intent ExitIntent = new Intent(Exit.this, Login.class);
        startActivity(ExitIntent);
    }
}
