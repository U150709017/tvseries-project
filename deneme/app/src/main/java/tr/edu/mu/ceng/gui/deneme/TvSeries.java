package tr.edu.mu.ceng.gui.deneme;

public class TvSeries {
    private int imageId;

    public TvSeries(int imageId){
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId){
        this.imageId = imageId;
    }
}
